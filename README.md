
# Let's Cook

### Introduction
 A software to streamline our operation and fulfilment functions at HelloChef and help decide how much of each ingredient needed on a weekly basis.
### Setting Up

#### Prerequisites

- Docker, docker-composer
- Git

####  Setting up

- clone the project

    

    git clone https://samlinncon@bitbucket.org/samlinncon/letscook.git

    
    
- cd into the project folder

    

    cd letscook
    
- create .env file

  
    cp .env.example .env

    
- Initialize docker containers for the project

    

    docker-compose build app

    
- Run the application, this sets up composer, runs migrations and runs sample seeds

  
    bash start.sh

    
- Access your app using an api client such as postman on the url `http:127.0.0.1:8080`

#### Testing the api
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/568dff641c110516f816)

Base URL: `http:127.0.0.1:8080`

##### Create Ingredient

API endpoint `/api/create-ingredient` 

Sample request body: 
```json
{
 	"name" : "Onions",
 	"measure" : "gram",
 	"supplier" :"Samuel"
 }
```

Sample response:
```json
{
    "data": {
        "id": 11,
        "name": "onions",
        "measure": "gram",
        "supplier": "samuel"
    }
}
```

