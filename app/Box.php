<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    //
    protected $fillable = ['delivery_date','recipe_ids'];

    protected $casts = [
        'recipe_ids' => 'array',
    ];

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class,'box_recipe')->withTimestamps();
    }

}
