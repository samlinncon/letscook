<?php

namespace App\Http\Controllers;

use App\Box;
use App\Recipe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Recipe as RecipeResource;
use Illuminate\Support\Facades\DB;



class BoxController extends Controller
{
    //create a box
    public function createBox(Request $request){
        //validate recipe input
        $dateNow = Carbon::now()->addHours(48);
        $validator = Validator::make($request->all(), [
            'delivery_date' => 'required|date|date_format:Y-m-d|after:'.$dateNow,
            'recipe_ids' => 'required|array|exists:App\Recipe,id|between:1,4',
        ]);
        if ($validator->fails()) {
            $errorResponse = [];
            $errors = array_map(function ($value) {
                return implode(' ', $value);
            }, $validator->errors()->toArray());
            $errorKeys = $validator->errors()->keys();

            foreach ($errorKeys as $key)
            {
                $array = ['field' => $key, 'error' => $errors[$key]];
                array_push($errorResponse, $array);
            }

            return response()->json(['status' => 'error', 'message' => $errorResponse], 500);
        }

        try{
            //store the order box
            $deliveryDate =$request->input('delivery_date');
            $recipeIDs = $request->input('recipe_ids');
            $box = new Box;
            $box->delivery_date = $deliveryDate;
            $box->recipe_ids = $recipeIDs;
            $box->save();

            if($box){
                foreach ($recipeIDs as $recipeID){
                    $box->recipes()->attach($recipeID);

                }
            }

            $recipies = RecipeResource::collection(Recipe::paginate());

            return $recipies;


        }catch (\Exception $exception){
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
                'data' => $request->toArray()
            ], 500);
        }
    }

    //fetch the order ingredients
    public function fetchBoxIngredients(Request $request){
        $validator = Validator::make($request->all(), [
            'order_date' => 'required|date',
        ]);
        if ($validator->fails()) {
            $errorResponse = [];
            $errors = array_map(function ($value) {
                return implode(' ', $value);
            }, $validator->errors()->toArray());
            $errorKeys = $validator->errors()->keys();

            foreach ($errorKeys as $key)
            {
                $array = ['field' => $key, 'error' => $errors[$key]];
                array_push($errorResponse, $array);
            }

            return response()->json(['status' => 'error', 'message' => $errorResponse], 500);
        }
        $orderDate = $request->input('order_date');
        $dateRange = Carbon::parse($orderDate)->addDays(7)->toDateTimeString();

        //fetch boxes needed 7 days from now
        $boxes = Box::query()->whereBetween('delivery_date',[$orderDate,$dateRange])->get();

        if($boxes->isNotEmpty()){

            $results = [];
            foreach ($boxes as $box){
                $totalMeasures = DB::table('ingredient_recipe')->whereIn('recipe_id',$box['recipe_ids'])
                    ->join('ingredients','ingredient_recipe.ingredient_id','=','ingredients.id')
                    ->select('ingredient_recipe.ingredient_id AS ingredient_id',DB::raw('SUM(ingredient_recipe.amount) as ingredient_total'),'ingredients.name as name')
                    ->groupBy('ingredient_id')
                    ->get()->toArray();

                array_push($results,$totalMeasures);
            }

            $ingredients_collapsed = Arr::collapse($results);

            $ingredient_names = array_values(array_unique(Arr::pluck($ingredients_collapsed, 'name')));

            $ingredient_results = array_map(function($name) use ($ingredients_collapsed) {
                $amount = array_sum(Arr::pluck(Arr::where($ingredients_collapsed, function ($value) use ($name) {
                    return $value->name == $name;
                }), 'ingredient_total'));

                return ['name' => $name, 'amount' => $amount];
            }, $ingredient_names);

            return response()->json($ingredient_results);


        }else{
            //no upcoming orders in next 7 days
            return response()->json([
                'data' => [],
                'message' =>'No upcoming orders'
            ]);

        }


    }

    //filter box
    public function filterBoxDeliveries(Request $request){

        $validator = Validator::make($request->all(), [
            'delivery_date' => 'required|date|date_format:Y-m-d'
        ]);
        if ($validator->fails()) {
            $errorResponse = [];
            $errors = array_map(function ($value) {
                return implode(' ', $value);
            }, $validator->errors()->toArray());
            $errorKeys = $validator->errors()->keys();

            foreach ($errorKeys as $key)
            {
                $array = ['field' => $key, 'error' => $errors[$key]];
                array_push($errorResponse, $array);
            }

            return response()->json(['status' => 'error', 'message' => $errorResponse], 500);
        }
        try{
            $deliveryDate = $request->input('delivery_date');
            $boxDeliveryFilterData = Box::query()->where('delivery_date',$deliveryDate)->select('id as boxId','delivery_date')->get()->toArray();

            if($boxDeliveryFilterData){
                return response()->json($boxDeliveryFilterData);

            }else{
                //no upcoming orders in next 7 days
                return response()->json([
                    'data' => [],
                    'message' =>'No delivery orders'
                ]);
            }


        }catch (\Exception $exception){
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
                'data' => $request->toArray()
            ], 500);
        }
    }
}
