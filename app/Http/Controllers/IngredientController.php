<?php

namespace App\Http\Controllers;

use App\Ingredient;
use Illuminate\Http\Request;
use App\Http\Resources\Ingredient as IngredientResource;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class IngredientController extends Controller
{
    //create an ingredient
    public function createIngredient(Request $request){

        //validate user input
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'measure' => 'required',
            'supplier' => 'required'
        ]);
        if ($validator->fails()) {
            $errorResponse = [];
            $errors = array_map(function ($value) {
                return implode(' ', $value);
            }, $validator->errors()->toArray());
            $errorKeys = $validator->errors()->keys();

            foreach ($errorKeys as $key)
            {
                $array = ['field' => $key, 'error' => $errors[$key]];
                array_push($errorResponse, $array);
            }

            return response()->json(['status' => 'error', 'message' => $errorResponse], 500);
        }

        try{
            //collect user input
            $name = $request->input('name');
            $measure = $request->input('measure');
            $supplier = $request->input('supplier');

            $ingredient = new Ingredient;
            $ingredient->name =strtolower($name);
            $ingredient->measure = $measure;
            $ingredient->supplier = strtolower($supplier);
            $ingredient->save();

            return new IngredientResource($ingredient);


        }catch (\Exception $exception){
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
                'data' => $request->toArray()
            ], 500);
        }

    }

    //fetch a list of ingredients available
    public function fetchIngredients(){
        $ingredients = IngredientResource::collection(Ingredient::paginate());
        return $ingredients;
    }


    //filter suppliers based on ingredients
    public function filterIngredientsSuppliers(Request $request){
        $ingredient = $request->input('ingredients');

        $ingredientsSuppliers = DB::table('ingredients')->select('name','supplier')->get()->toArray();

        $filtered_supplier = array_values(Arr::where($ingredientsSuppliers,function ($value,$key)use($ingredient){
            return Str::contains((strtolower($value->name)), explode(' ', strtolower($ingredient)));
        }));

        if(empty($filtered_supplier)){
            return response()->json([
                'status' =>"success",
                'message' =>"No supplier for the ingredient"
            ]);
        }else{
            return response()->json([
                'message' => 'success',
                'data'=> $filtered_supplier
         ]);
        }

    }
}
