<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Recipe as RecipeResource;


class RecipeController extends Controller
{
    //create a recipe
    public function createRecipe(Request $request){
     //validate recipe input
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'ingredients' => 'required|array',
        ]);

        if ($validator->fails()) {
            $errorResponse = [];
            $errors = array_map(function ($value) {
                return implode(' ', $value);
            }, $validator->errors()->toArray());
            $errorKeys = $validator->errors()->keys();

            foreach ($errorKeys as $key)
            {
                $array = ['field' => $key, 'error' => $errors[$key]];
                array_push($errorResponse, $array);
            }

            return response()->json(['status' => 'error', 'message' => $errorResponse], 500);
        }

        try{

            //collect user input
            $name = $request->input('name');
            $description = $request->input('description');
            $ingredients = $request->input('ingredients');

            $recipe = new Recipe;
            $recipe->name  = $name;
            $recipe->description  = $description;
            $recipe->ingredients = $ingredients;
            $recipe->save();

            if($recipe){
                foreach ($ingredients as $ingredient){
                    $amount = $ingredient['amount'];
                    $ingredientId =  $ingredient['ingredient_id'];
                                    $recipe->ingredients()->attach($ingredientId,['amount' => $amount]);

                }

            }

            return new RecipeResource($recipe);


        }catch (\Exception $exception){
            return response()->json([
                'status' => 'error',
                'message' => $exception->getMessage(),
                'data' => $request->toArray()
            ], 500);
        }
    }

    //fetch recipes created
    public function fetchRecipes(){
        $recipies = RecipeResource::collection(Recipe::paginate());
        return $recipies;
    }

}
