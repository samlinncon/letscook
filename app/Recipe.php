<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    //
    protected $fillable = ['name','description','ingredients'];

    protected $casts = [
        'ingredients' => 'array',
    ];

    public function ingredients(){
        return $this->belongsToMany(Ingredient::class,'ingredient_recipe')->withTimestamps()->withPivot('amount');
    }
}
