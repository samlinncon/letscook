<?php

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seed boxes with recipes
        $startDate = Carbon::now();
        $endDate = Carbon::now()->addMonth();
        $faker = Faker::create();

        foreach (range(1,10) as $index) {
            $recipes = [];
            foreach (range(1,rand(1,4)) as $key){

                array_push($recipes, (rand(1, 10)));
            }
            $randomDate = Carbon::createFromTimestamp(rand($endDate->timestamp, $startDate->timestamp))->format('Y-m-d');

            $box_id = DB::table('boxes')->insertGetId([
                'delivery_date' => $randomDate,
                'recipe_ids' => json_encode($recipes),
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            foreach ($recipes as $recipe){
                DB::table('box_recipe')->insertGetId([
                    'box_id' => $box_id,
                    'recipe_id' =>$recipe, //
                    'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
