<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;



class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seed ingredients using name,measure,supplier
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('ingredients')->insert([
                'name' => "ING".$faker->word,
                'measure' => $faker->randomElement(['KiloGram','Grams','Pieces']),
                'supplier' => $faker->name,
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

    }
}
