<?php

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            $ingres = [];
            foreach (range(1,rand(1,5)) as $key){

                array_push($ingres, ["ingredient_id" => (rand(1, 10)), "amount" => rand(1, 10)]);
            }
            $recipe_id = DB::table('recipes')->insertGetId([
                'name' => "ING".$faker->word,
                'description' => $faker->paragraph,
                'ingredients' =>json_encode($ingres ),
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            foreach ($ingres as $ingre){
                DB::table('ingredient_recipe')->insertGetId([
                    'recipe_id' => $recipe_id,
                    'ingredient_id' => $ingre['ingredient_id'],
                    'amount' =>  $ingre['amount'],
//
                    'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
