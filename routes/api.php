<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//ingredients
Route::post('create-ingredient','IngredientController@createIngredient');
Route::get('list-ingredients','IngredientController@fetchIngredients');
Route::post('filter-ingredient-supplier','IngredientController@filterIngredientsSuppliers');

//recipes
Route::post('create-recipe','RecipeController@createRecipe');
Route::get('list-recipes','RecipeController@fetchRecipes');

//boxes
Route::post('create-box','BoxController@createBox');

//fetch ingredients ordered
Route::post('fetch-box-ingredients','BoxController@fetchBoxIngredients');
Route::post('filter-box-deliveries','BoxController@filterBoxDeliveries');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

